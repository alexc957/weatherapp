package com.example.weatherapp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainWeatherFragment : Fragment() {
    lateinit var  cityName: TextView
    lateinit var weatherName: TextView
    lateinit var searchButton: TextView
    lateinit var weatherImage: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityName = view.findViewById(R.id.cityName)
        searchButton = view.findViewById(R.id.searchButton)
        weatherName = view.findViewById(R.id.weather_response)
        weatherImage = view.findViewById(R.id.weatherImage)


        searchButton.setOnClickListener { getWeather() }


    }

    fun getWeather() {
        val weatherService = WeatherService.instance
        val weatherCall = weatherService.getWeather(city = cityName.text.toString())
        weatherCall.enqueue(object : Callback<WeatherResponse>{
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                TODO("SDF")
            }

            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {

                 //To change body of created functions use File | Settings | File Templates.
                Log.d("WEATHER",response.message())
                val weatherList = response.body()
                val firstWeather = weatherList?.weather?.get(0)
                weatherName.text = firstWeather?.name ?: "Error"

                val iconUrl = "http://openweathermap.org/img/w/${firstWeather?.icon?:"10d"}.png"

                Glide.with(context!!)
                    .load(iconUrl)
                    .into(weatherImage)


            }

        })
    }


}
